﻿from viewer import *
import time
figType = "T"
figure = readFile("cube.m3d",figType)

dicc_size = {"T":4,"P":5,"R":6}

figure.view()

arch = open("../Examples/TetraedroC1.txt.points")

for linea in arch:
	xrot = 0
	yrot = 0
	linea = linea.strip()
	data = linea.split()[1:]
	for i in range(dicc_size[figType]):
	
		figure.vertices[i].x = float(data[i].split(";")[0])
		figure.vertices[i].y = float(data[i].split(";")[1])
		figure.vertices[i].z = float(data[i].split(";")[2])

	raw_input()
	xrot = 0
	yrot = 0
	for i in range(0, len(figure.vertices)):
		figure.vertices[i] = figure.vertices[i].rotateX(-xrot / 2).rotateY(-yrot / 2).rotateZ(0)
	figure.view()
	figure.save()
	time.sleep(0.1)

# Guarda como archivo .off
#figure.writeoff("cube.off")

# Restaura a la configuracion inicial de la figura (cubo)
#figure.restore()
#figure.writem3d("cubeRestored.m3d")

arch.close()
