# JENS 

## About
This scripts excecute tests for meshes located in `/meshes` folder. Example:
if mesh file is `pyramid.m3d`, the `<mesh_name>` will be `pyramid`.

Later, save output files with quality measures in `/output` folder in format `<mesh_name><TEST>.txt` and `<mesh_name><TEST>.txt.points` where `<TEST>` is a letter starting by `A`. 

Finally, Plotter (located in `/Plotter` folder) will take output results and will generate PDF files with graphics of test. This files will be generated in `/graphics` folder with format `<mesh_name><ELEMENT><TEST>R.pdf`. 

*Example1:* the file `pyramid_PBR.pdf` is graphic of results from mesh located in `/meshes/pyramid.m3d` for Test B of Pyramids. 

*Example2:* the file `mesh1_RAR.pdf` is graphic of results from mesh located on `/meshes/mesh1.m3d` for TEST A of Prisms.

Plotter is written in Python and requires `numpy` and `matplotlib` modules.

## How to run

tl;dr Excecute script bash

> `run_all_tests.sh <tetrahedron_mesh_name> <prism_mesh_name> <pyramid_mesh_name>`

This script will complile, run tests and generate graphics.

## How to run all tests for each element

From main directory, complile code with cmake and make

> `cmake ./src`

> `make`

Then, use options to run tests:

- `run_prism_tests.sh <prism_mesh_name>`
- `run_pyramid_tests.sh <pyramid_mesh_name>`
- `run_tetrahedron_tests.sh <tetrahedron_mesh_name>`

This scripts runs only tests for respective element and generate graphics associated.

## How to run manually

From main directory, complile code with cmake and make

> `cmake ./src`

> `make`

Use `jens` excecutable file with parameters associated to tests Example:

> `./jens -pa ./meshes/mesh.m3d ./output/meshA.txt 60`

Will run test A of Pyramid for mesh `./meshes/mesh.m3d` and will save results on `./output/meshA.txt` using `60` iterations for node(s) movement defined for test.

Then, call Plotter to generate graphic of this result. Example:

> `python3 ./Plotter/plotter.py ./graphics/prefix_ "./output/meshA.txt"` 

Will read `./output/meshA.txt` file and will generate plot in `./graphics/` folder. The name of file generated will be `prefix_meshAR.pdf`.