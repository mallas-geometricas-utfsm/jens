/**************************************************************************/
/*                                                                        */
/*                  Jens                                                  */
/*                                                                        */
/**************************************************************************/
/* Allows to display Jacobian Information of a mesh in .m3d format        */
/* Written by Claudio Lobos (clobos@inf.utfsm.cl) 2015					  */
/* UNIVERSIDAD TECNICA FEDERICO SANTA MARIA								  */
/**************************************************************************/

/*
 <JENS: this program computes the Element Normalized Scaled Jacobian (Jens)>
 
 Copyright (C) <2015>  <Claudio Lobos>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>
 */

#include "DeformedTetrahedronComplement.h"

DeformedTetrahedronComplement::DeformedTetrahedronComplement(vector<int> &pts):Element(pts) {
    type='2';
    jens_constant = 1.0;
    tetsQuantity.resize(7,1);
    tetsQuantity[0]=10;
    tetsQuantity[2]=10;
    tetsQuantity[4]=4;
    tetsQuantity[5]=4;
    tetsQuantity[6]=4;
}

DeformedTetrahedronComplement::~DeformedTetrahedronComplement(){
    
}

double DeformedTetrahedronComplement::getAspectRatio(vector<Point3D> &points) {

    return 0;

    /*vector<Point> lpoints,mid;
     Point midbase;
     vector<double> hs;
     vector<int> base,pointindex;
     double min,max;
     pointindex = element->getPoints();
     
     lpoints.reserve(5);
     mid.reserve(4);
     hs.reserve(3);
     
     base = element->getFacePoints(0);
     
     for(int i=0;i<4;i++){
     lpoints.push_back(points[base[i]].getPoint());
     }
     
     lpoints.push_back(points[pointindex[4]].getPoint());
     for(int i=0;i<4;i++){
     midbase+=lpoints[i];
     }
     midbase/=4;
     
     hs.push_back((lpoints[4]-midbase).norm());
     min = max = hs[0];
     
     for(int i=0;i<4;i++){
     int next = (i+1)%4;
     Point aux = lpoints[i]+lpoints[next];
     aux/=2;
     mid.push_back(aux);
     }
     hs.push_back((mid[0] - mid[2]).norm());
     hs.push_back((mid[1] - mid[3]).norm());
     for(int i=1;i<3;i++){
     if(hs[i] < min)
     min = hs[i];
     if(hs[i] > max)
     max = hs[i];
     }
     return min / max;*/
}


double DeformedTetrahedronComplement::getElementJS(vector<Point3D> &pts){
    
    double worst = 2;
    vector<double> auxjs = getJS(pts);
    
    for (unsigned int i=0; i<auxjs.size(); i++) {
        if (worst>auxjs[i]) {
            worst = auxjs[i];
        }
    }
    return worst;
    
}

double DeformedTetrahedronComplement::getElementJENS(vector<Point3D> &pts){
    
    double worst = 2;
    vector<double> auxjens = getJENS(pts);
    
    for (unsigned int i=0; i<auxjens.size(); i++) {
        if (worst>auxjens[i]) {
            worst = auxjens[i];
        }
    }
    return worst;
}

vector<double> DeformedTetrahedronComplement::getJS(vector<Point3D> &pts){
    
    vector<double> auxjs,js(points.size(),0);
    
    auxjs = Element::getJS(pts);
    
    /*double minpos=2, maxneg=-2, apice =0;
    
    bool positive= false;
    
    for (unsigned int i=4; i<auxjs.size(); i++) {
        if (auxjs[i]>=0 && auxjs[i]<minpos) {
            minpos = auxjs[i];
            positive = true;
        }
        if (auxjs[i]<0 && auxjs[i]>maxneg) {
            maxneg = auxjs[i];
        }
    }
    
    if (positive) {
        js[4] = minpos;
    }
    else {
        js[4] = maxneg;
    }
    
    for (unsigned int i=0; i<4; i++) {
        js[i] = auxjs[i];
    }*/

    unsigned int point = 0;
    for (unsigned int i=0; i < auxjs.size(); ++i){
        if (tetsQuantity[point]==1){
            js[point] = auxjs[i];
            point++;
        }else{
            double minpos=2, maxneg=-2, apice =0;
            bool positive = false;
            for(unsigned int j=0; j<tetsQuantity[point]; ++j,++i){
                if (auxjs[i]>=0 && auxjs[i]<minpos) {
                    minpos = auxjs[i];
                    positive = true;
                }
                if (auxjs[i]<0 && auxjs[i]>maxneg) {
                    maxneg = auxjs[i];
                }
            }
            if (positive) {
                js[point] = minpos;
            }
            else {
                js[point] = maxneg;
            }
            point++;
            i--;
        }
    }

    
    return js;
}

vector<double> DeformedTetrahedronComplement::getJENS(vector<Point3D> &pts){
    
    vector<double> auxjens,jens(points.size(),0);
    
    auxjens = Element::getJENS(pts);
    
    /*double minpos=2, maxneg=-2, apice =0;
    
    bool positive= false;
    
    for (unsigned int i=0; i<4; i++) {
        jens[i] = auxjens[i];
    }
    
    for (unsigned int i=4; i<auxjens.size(); i++) {
        if (auxjens[i]>=0 && auxjens[i]<minpos) {
            minpos = auxjens[i];
            positive = true;
        }
        if (auxjens[i]<0 && auxjens[i]>maxneg) {
            maxneg = auxjens[i];
        }
    }
    
    if (positive) {
        jens[4] = minpos;
    }
    else {
        jens[4] = maxneg;
    }*/

    unsigned int point = 0;
    for (unsigned int i=0; i < auxjens.size(); ++i){
        if (tetsQuantity[point]==1){
            jens[point] = auxjens[i];
            point++;
        }else{
            double minpos=2, maxneg=-2, apice =0;
            bool positive = false;
            for(unsigned int j=0; j<tetsQuantity[point]; ++j,++i){
                if (auxjens[i]>=0 && auxjens[i]<minpos) {
                    minpos = auxjens[i];
                    positive = true;
                }
                if (auxjens[i]<0 && auxjens[i]>maxneg) {
                    maxneg = auxjens[i];
                }
            }
            if (positive) {
                jens[point] = minpos;
            }
            else {
                jens[point] = maxneg;
            }
            point++;
            i--;
        }
    }
    
    return jens;
}

vector<vector<unsigned int> > DeformedTetrahedronComplement::getTetras() {
    
    vector<vector<unsigned int> > tets;
    tets.reserve(34);
    unsigned int comb4[4] = {0,0,0,0};
    unsigned int comb5[5] = {0,0,0,0,0};
    unsigned int indexP1,indexP2,indexP3;
    unsigned int initP = 0;
    

    //point 0 init
    comb5[0]=3;
    comb5[1]=5;
    comb5[2]=6;
    comb5[3]=4;
    comb5[4]=1;
    initP = 0;
    for(indexP3=2; indexP3 < 5;indexP3++){
        for(indexP2=1; indexP2 < indexP3;indexP2++){
            for(indexP1=0; indexP1 < indexP2; indexP1++){
                tets.push_back(vector<unsigned int>(4,0));
                tets.back()[0]=points[initP];
                tets.back()[1]=points[comb5[indexP1]];
                tets.back()[2]=points[comb5[indexP2]];
                tets.back()[3]=points[comb5[indexP3]];
            }
        }
    }
    
    
    //point 1
    tets.push_back(vector<unsigned int>(4,0));
    tets.back()[0]=points[1];
    tets.back()[1]=points[0];
    tets.back()[2]=points[4];
    tets.back()[3]=points[2];
    
    
    //point 2 init
    comb5[0]=1;
    comb5[1]=4;
    comb5[2]=6;
    comb5[3]=5;
    comb5[4]=3;
    initP = 2;
    for(indexP3=2; indexP3 < 5;indexP3++){
        for(indexP2=1; indexP2 < indexP3;indexP2++){
            for(indexP1=0; indexP1 < indexP2; indexP1++){
                tets.push_back(vector<unsigned int>(4,0));
                tets.back()[0]=points[initP];
                tets.back()[1]=points[comb5[indexP1]];
                tets.back()[2]=points[comb5[indexP2]];
                tets.back()[3]=points[comb5[indexP3]];
            }
        }
    }
    
    //point 3
    tets.push_back(vector<unsigned int>(4,0));
    tets.back()[0]=points[3];
    tets.back()[1]=points[2];
    tets.back()[2]=points[5];
    tets.back()[3]=points[0];
    
    //point 4 init
    comb4[0]=0;
    comb4[1]=6;
    comb4[2]=2;
    comb4[3]=1;
    initP = 4;
    for(indexP3=2; indexP3 < 4;indexP3++){
        for(indexP2=1; indexP2 < indexP3;indexP2++){
            for(indexP1=0; indexP1 < indexP2; indexP1++){
                tets.push_back(vector<unsigned int>(4,0));
                tets.back()[0]=points[initP];
                tets.back()[1]=points[comb4[indexP1]];
                tets.back()[2]=points[comb4[indexP2]];
                tets.back()[3]=points[comb4[indexP3]];
            }
        }
    }
    
    //point 5 init
    comb4[0]=0;
    comb4[1]=3;
    comb4[2]=2;
    comb4[3]=6;
    initP = 5;
    for(indexP3=2; indexP3 < 4;indexP3++){
        for(indexP2=1; indexP2 < indexP3;indexP2++){
            for(indexP1=0; indexP1 < indexP2; indexP1++){
                tets.push_back(vector<unsigned int>(4,0));
                tets.back()[0]=points[initP];
                tets.back()[1]=points[comb4[indexP1]];
                tets.back()[2]=points[comb4[indexP2]];
                tets.back()[3]=points[comb4[indexP3]];
            }
        }
    }
    
    //point 6 init
    comb4[0]=0;
    comb4[1]=5;
    comb4[2]=2;
    comb4[3]=4;
    initP = 6;
    for(indexP3=2; indexP3 < 4;indexP3++){
        for(indexP2=1; indexP2 < indexP3;indexP2++){
            for(indexP1=0; indexP1 < indexP2; indexP1++){
                tets.push_back(vector<unsigned int>(4,0));
                tets.back()[0]=points[initP];
                tets.back()[1]=points[comb4[indexP1]];
                tets.back()[2]=points[comb4[indexP2]];
                tets.back()[3]=points[comb4[indexP3]];
            }
        }
    }

    return tets;
}

vector<unsigned int> DeformedTetrahedronComplement::getTetra(int index){
    // No veo que se use, por eso la deje comentada.
    cerr << "getTetra(). No veo que se use, por eso esta comentada en el codigo." << endl;
    vector<unsigned int> t(4,0);
    return t;
   /* vector<unsigned int> t(4,0);
    if(index==0){
        t[0]=points[0];
        t[1]=points[1];
        t[2]=points[3];
        t[3]=points[4];
    }
    else if(index==1){
        t[0]=points[1];
        t[1]=points[2];
        t[2]=points[0];
        t[3]=points[4];
    }
    else if(index==2){
        t[0]=points[2];
        t[1]=points[3];
        t[2]=points[1];
        t[3]=points[4];
    }
    else if(index==3){
        t[0]=points[3];
        t[1]=points[0];
        t[2]=points[2];
        t[3]=points[4];
    }
    else if(index==4){
        t[0]=points[4];
        t[1]=points[0];
        t[2]=points[3];
        t[3]=points[1];
    }
    else if(index==5){
        t[0]=points[4];
        t[1]=points[1];
        t[2]=points[0];
        t[3]=points[2];
    }
    else if(index==6){
        t[0]=points[4];
        t[1]=points[2];
        t[2]=points[1];
        t[3]=points[3];
    }
    else if(index==7){
        t[0]=points[4];
        t[1]=points[3];
        t[2]=points[2];
        t[3]=points[0];
    }
    return t;*/
}
