/**************************************************************************/
/*                                                                        */
/*                  Jens                                                  */
/*                                                                        */
/**************************************************************************/
/* Allows to display Jacobian Information of a mesh in .m3d format        */
/* Written by Claudio Lobos (clobos@inf.utfsm.cl) 2015					  */
/* UNIVERSIDAD TECNICA FEDERICO SANTA MARIA								  */
/**************************************************************************/

/*
 <JENS: this program computes the Element Normalized Scaled Jacobian (Jens)>
 
 Copyright (C) <2015>  <Claudio Lobos>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>
 */

#include "Quality.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
Quality::Quality(){
    
}


//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
Quality::~Quality(){
    
}


//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void Quality::execute_JS(vector<Point3D> &p, vector<Element *> &e){
    
    if (e.empty()){
        cout << "no elements\n";
        return;
    }
    vector<int> histo (20,0);
    double worst = 2, total = 0;
    
    vector<double> min_vals_type (4,2), max_vals_type (4,-2), ave_vals_type (4,0);
    vector<unsigned int> ele_quantity (4,0);
    
    unsigned int type = 0;
    
    for (int i=0; i<(int)e.size(); i++) {
        double qua = e[i]->getElementJS(p);
        total += qua;
        bool print = false;
        if(worst > qua)
            worst = qua;
        if(qua<0){
            histo[0]++;
            print = true;
        }
        else if(qua<0.033333){
            histo[1]++;
            print = true;
        }
        else if(qua<0.05)
            histo[2]++;
        else if(qua<0.1)
            histo[3]++;
        else if(qua<0.15)
            histo[4]++;
        else if(qua<0.2)
            histo[5]++;
        else if(qua<0.25)
            histo[6]++;
        else if(qua<0.3)
            histo[7]++;
        else if(qua<0.35)
            histo[8]++;
        else if(qua<0.4)
            histo[9]++;
        else if(qua<0.45)
            histo[10]++;
        else if(qua<0.5)
            histo[11]++;
        else if(qua<0.55)
            histo[12]++;
        else if(qua<0.6)
            histo[13]++;
        else if(qua<0.65)
            histo[14]++;
        else if(qua<0.7)
            histo[15]++;
        else if(qua<0.85)
            histo[16]++;
        else if(qua<0.9)
            histo[17]++;
        else if(qua<0.95)
            histo[18]++;
        else if(qua<=1)
            histo[19]++;


        /*else{
            cerr << "Quality quality value at Quality::surfaceQuality: ";
            cerr << qua << endl;
        }*/
        
        if (print) {
            cout << i << " " << *(e[i]) << " 1000.0 0.45 1.0\n";
        }
        
        //find the type
        if(e[i]->getCharType()=='H') {
            type=0;
        }
        else if(e[i]->getCharType()=='R') {
            type=1;
        }
        else if(e[i]->getCharType()=='P') {
            type=2;
        }
        else if(e[i]->getCharType()=='T') {
            type=3;
        }
        
        //update min and max per element type.
        if(min_vals_type[type] > qua){
            min_vals_type[type] = qua;
        }
        if(qua > max_vals_type[type]){
            max_vals_type[type] = qua;
        }
        
        ave_vals_type[type] += qua;
        ele_quantity[type]++;
        
    }
    cout << "negative: " << histo[0] << endl;
    cout << "<0.0333 : " << histo[1] << endl;
    float step = 0.05;
    for(int i=2;i<19;i++) {
        cout << "<" << step << "\t:" << histo[i] << endl;
        step+=0.05;
    }
    cout << "<1 " << histo[19] << endl;
    cout << "total: " << e.size() << endl;
    cout << "worst quality " << worst << endl;
    cout << "average quality " << total/e.size() << endl;
    
    cout << "\ntikz format\n";
    cout << "(-1," << histo[0] << ")\n";
    cout << "(0.03," << histo[1] << ")\n";
    step = 0.05;
    for(int i=2;i<19;i++){
        cout << "(" << step << "," << histo[i] << ")\n";
        step+=0.05;
    }
    cout << "(1," << histo[19] << ")\n";
    
    //print range of quality per element type
    cout << " > Quality per element type: Hex, Pri, Pyr, Tet respectively:\n";
    for (unsigned int i=0; i<min_vals_type.size(); i++) {
        if (ele_quantity[i]>=2) {
            cout << "[ " << min_vals_type[i] << "," << max_vals_type[i] << "] average: ";
            cout << ave_vals_type[i]/ele_quantity[i] << " (" << ele_quantity[i] << ")\n";
        }
        if (ele_quantity[i]==1) {
            cout << "[ " << ave_vals_type[i] << "," << ave_vals_type[i] << "] average: ";
            cout << ave_vals_type[i] << " (" << ave_vals_type[i] << ")\n";
        }
        else {
            cout << "no elements of this type\n";
        }
    }
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void Quality::execute_JENS(vector<Point3D> &p, vector<Element *> &e){
    
    if (e.empty()){
        cout << "no elements\n";
        return;
    }
    vector<int> histo (20,0);
    double worst = 2, total = 0;
    
    vector<double> min_vals_type (4,2), max_vals_type (4,-2), ave_vals_type (4,0);
    vector<unsigned int> ele_quantity (4,0);
    
    unsigned int type = 0;
    
    for (int i=0; i<(int)e.size(); i++) {
        double qua = e[i]->getElementJENS(p);
        total += qua;
        bool print = false;
        if(worst > qua)
            worst = qua;
        if(qua<0){
            histo[0]++;
            print = true;
        }
        else if(qua<0.033333){
            histo[1]++;
            print = true;
        }
        else if(qua<0.05)
            histo[2]++;
        else if(qua<0.1)
            histo[3]++;
        else if(qua<0.15)
            histo[4]++;
        else if(qua<0.2)
            histo[5]++;
        else if(qua<0.25)
            histo[6]++;
        else if(qua<0.3)
            histo[7]++;
        else if(qua<0.35)
            histo[8]++;
        else if(qua<0.4)
            histo[9]++;
        else if(qua<0.45)
            histo[10]++;
        else if(qua<0.5)
            histo[11]++;
        else if(qua<0.55)
            histo[12]++;
        else if(qua<0.6)
            histo[13]++;
        else if(qua<0.65)
            histo[14]++;
        else if(qua<0.7)
            histo[15]++;
        else if(qua<0.85)
            histo[16]++;
        else if(qua<0.9)
            histo[17]++;
        else if(qua<0.95)
            histo[18]++;
        else if(qua<=1)
            histo[19]++;
        
        /*else{
            cerr << "Quality quality value at Quality::surfaceQuality: ";
            cerr << qua << endl;
        }*/
        
        if (print) {
            cout << i << "\n";// << " " << *(e[i]) << " 1000.0 0.45 1.0\n";
        }
        
        //find the type
        if(e[i]->getCharType()=='H') {
            type=0;
        }
        else if(e[i]->getCharType()=='R') {
            type=1;
        }
        else if(e[i]->getCharType()=='P') {
            type=2;
        }
        else if(e[i]->getCharType()=='T') {
            type=3;
        }
        
        //update min and max per element type.
        if(min_vals_type[type] > qua){
            min_vals_type[type] = qua;
        }
        if(qua > max_vals_type[type]){
            max_vals_type[type] = qua;
        }
        
        ave_vals_type[type] += qua;
        ele_quantity[type]++;
        
    }

    cout << "negative: " << histo[0] << endl;
    cout << "<0.0333 : " << histo[1] << endl;
    float step = 0.05;
    for(int i=2;i<19;i++) {
        cout << "<" << step << "\t:" << histo[i] << endl;
        step+=0.05;
    }
    cout << "<1 " << histo[19] << endl;
    cout << "total: " << e.size() << endl;
    cout << "worst quality " << worst << endl;
    cout << "average quality " << total/e.size() << endl;
    
    cout << "\ntikz format\n";
    cout << "(-1," << histo[0] << ")\n";
    cout << "(0.03," << histo[1] << ")\n";
    step = 0.05;
    for(int i=2;i<19;i++){
        cout << "(" << step << "," << histo[i] << ")\n";
        step+=0.05;
    }
    cout << "(1," << histo[19] << ")\n";

        
    //print range of quality per element type
    cout << " > Quality per element type: Hex, Pri, Pyr, Tet respectively:\n";
    for (unsigned int i=0; i<min_vals_type.size(); i++) {
        if (ele_quantity[i]>=2) {
            cout << "[ " << min_vals_type[i] << "," << max_vals_type[i] << "] average: ";
            cout << ave_vals_type[i]/ele_quantity[i] << " (" << ele_quantity[i] << ")\n";
        }
        if (ele_quantity[i]==1) {
            cout << "[ " << ave_vals_type[i] << "," << ave_vals_type[i] << "] average: ";
            cout << ave_vals_type[i] << " (" << ele_quantity[i] << ")\n";
        }
        else {
            cout << "no elements of this type\n";
        }
    }
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void Quality::execute_JRATIO(vector<Point3D> &p, vector<Element *> &e){
    
    if (e.empty()){
        cout << "no elements\n";
        return;
    }
    vector<int> histo (20,0);
    double worst = 2, total = 0;
    
    vector<double> min_vals_type (4,2), max_vals_type (4,-2), ave_vals_type (4,0);
    vector<unsigned int> ele_quantity (4,0);
    
    unsigned int type = 0;
    
    for (int i=0; i<(int)e.size(); i++) {
        double qua = e[i]->getElementJRATIO(p);
        total += qua;
        bool print = false;
        if(worst > qua)
            worst = qua;
        if(qua<0){
            histo[0]++;
            print = true;
        }
        else if(qua<0.033333){
            histo[1]++;
            print = true;
        }
        else if(qua<0.05)
            histo[2]++;
        else if(qua<0.1)
            histo[3]++;
        else if(qua<0.15)
            histo[4]++;
        else if(qua<0.2)
            histo[5]++;
        else if(qua<0.25)
            histo[6]++;
        else if(qua<0.3)
            histo[7]++;
        else if(qua<0.35)
            histo[8]++;
        else if(qua<0.4)
            histo[9]++;
        else if(qua<0.45)
            histo[10]++;
        else if(qua<0.5)
            histo[11]++;
        else if(qua<0.55)
            histo[12]++;
        else if(qua<0.6)
            histo[13]++;
        else if(qua<0.65)
            histo[14]++;
        else if(qua<0.7)
            histo[15]++;
        else if(qua<0.85)
            histo[16]++;
        else if(qua<0.9)
            histo[17]++;
        else if(qua<0.95)
            histo[18]++;
        else if(qua<=1)
            histo[19]++;
        
        /*else{
            cerr << "Quality quality value at Quality::surfaceQuality: ";
            cerr << qua << endl;
        }*/
        
        if (print) {
            cout << i << "\n";// << " " << *(e[i]) << " 1000.0 0.45 1.0\n";
        }
        
        
        //find the type
        if(e[i]->getCharType()=='H') {
            type=0;
        }
        else if(e[i]->getCharType()=='R') {
            type=1;
        }
        else if(e[i]->getCharType()=='P') {
            type=2;
        }
        else if(e[i]->getCharType()=='T') {
            type=3;
        }
        
        //update min and max per element type.
        if(min_vals_type[type] > qua){
            min_vals_type[type] = qua;
        }
        if(qua > max_vals_type[type]){
            max_vals_type[type] = qua;
        }
        
        ave_vals_type[type] += qua;
        ele_quantity[type]++;
        
    }
    cout << "negative: " << histo[0] << endl;
    cout << "<0.0333 : " << histo[1] << endl;
    float step = 0.05;
    for(int i=2;i<19;i++) {
        cout << "<" << step << "\t:" << histo[i] << endl;
        step+=0.05;
    }
    cout << "<1 " << histo[19] << endl;
    cout << "total: " << e.size() << endl;
    cout << "worst quality " << worst << endl;
    cout << "average quality " << total/e.size() << endl;
    
    cout << "\ntikz format\n";
    cout << "(-1," << histo[0] << ")\n";
    cout << "(0.03," << histo[1] << ")\n";
    step = 0.05;
    for(int i=2;i<19;i++){
        cout << "(" << step << "," << histo[i] << ")\n";
        step+=0.05;
    }
    cout << "(1," << histo[19] << ")\n";

        
    //print range of quality per element type
    cout << " > Quality per element type: Hex, Pri, Pyr, Tet respectively:\n";
    for (unsigned int i=0; i<min_vals_type.size(); i++) {
        if (ele_quantity[i]>=2) {
            cout << "[ " << min_vals_type[i] << "," << max_vals_type[i] << "] average: ";
            cout << ave_vals_type[i]/ele_quantity[i] << " (" << ele_quantity[i] << ")\n";
        }
        if (ele_quantity[i]==1) {
            cout << "[ " << ave_vals_type[i] << "," << ave_vals_type[i] << "] average: ";
            cout << ave_vals_type[i] << " (" << ele_quantity[i] << ")\n";
        }
        else {
            cout << "no elements of this type\n";
        }
    }
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void Quality::execute_all(vector<Point3D> &p, vector<Element *> &e){
    
    for (unsigned int i=0; i<e.size(); i++) {
        vector<double> js = e[i]->getJS(p);
        vector<double> jens = e[i]->getJENS(p);
        vector<double> jratio = e[i]->getJRatios(p);
        
        double worst_js = 2, worst_jens=2, worst_jrat = 2;
        
        for (unsigned int j=0; j<jens.size(); j++) {
            cout << "node " << j << " jens " << jens[j] << " js " << js[j];
            cout << " jratio " << jratio[j] << "\n";
            if (worst_jens > jens[j]) {
                worst_jens = jens[j];
            }
            if (worst_js > js[j]) {
                worst_js = js[j];
            }
            if (worst_jrat>jratio[j]) {
                worst_jrat=jratio[j];
            }
        }
        cout << "Element jens " << worst_jens << " js " << worst_js;
        cout << " jratio " << worst_jrat;
        if (e[i]->getCharType()=='T') {
            cout << " ar " << e[i]->getAspectRatio(p);
        }
        cout << "\n-------------------\n";
    }
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void Quality::execute_allAR(vector<Point3D> &p, vector<Element *> &e){
    
    for (unsigned int i=0; i<e.size(); i++) {
        cout << "ARf " << e[i]->getAspectRatio(p) << " ARe ";
        cout << e[i]->getEdgeAspectRatio(p) << "\n";
    }
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
double Quality::get_JS(vector<Point3D> &p, vector<Element *> &e){
    
    if (e.empty()){
        return -2;
    }
    
    double worst = 2;
    
    
    for (int i=0; i<(int)e.size(); i++) {
        double qua = e[i]->getElementJS(p);
        if(worst > qua)
            worst = qua;
    }
    return worst;
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
double Quality::get_JENS(vector<Point3D> &p, vector<Element *> &e){
    
    if (e.empty()){
        return -2;
    }
    
    double worst = 2;
    
    
    for (int i=0; i<(int)e.size(); i++) {
        double qua = e[i]->getElementJENS(p);
        if(worst > qua)
            worst = qua;
    }
    return worst;
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
double Quality::get_ARG(vector<Point3D> &p, vector<Element *> &e){
    
    if (e.empty()){
        return -2;
    }
    
    double worst = 2;
    
    
    for (int i=0; i<(int)e.size(); i++) {
        double qua = e[i]->getAspectRatio(p);
        if(worst > qua)
            worst = qua;
    }
    return worst;
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
double Quality::get_JRATIO(vector<Point3D> &p, vector<Element *> &e){
    
    if (e.empty()){
        cout << "no elements\n";
        return -2;
    }
    double worst = 2;
    
    for (int i=0; i<(int)e.size(); i++) {
        double qua = e[i]->getElementJRATIO(p);
        if(worst > qua)
            worst = qua;
    }
    
    return worst;
}
