#ifndef Test_h
#define Test_h 1

#include <string>
#include <cctype>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <list>
#include <math.h>
#include "Quality.h"

using Clobscode::Point3D;

using namespace std;

class Test {
    
public:
    
    Test();
    
    virtual ~Test();
    
    virtual void writeGraphData(string name, vector<string> labels, vector<vector<double> > datos);
    
    virtual void writePointData(string name,vector<Point3D> &points, vector<vector<Point3D> > &pts, int &p,bool arg,int it);
    virtual void writePointData(string name,vector<Point3D> &points, vector<vector<Point3D> > &pts, int &p1,int &p2,bool arg,int it);
    virtual void writePointData(string name,vector<Point3D> &points, vector<vector<Point3D> > &pts, int &p1,int &p2,int &p3,bool arg,int it);
    
    virtual vector<vector<double> > get_PointsQuality(vector<Point3D> &points, vector<Element *> &elements, vector<int> pts, vector<vector<Point3D> > coordinates, bool arg);
    
    virtual void execute_LineTest(vector<Point3D> &points, vector<Element *> &elements, string name, int point, Point3D line, double epsilon, int iterations, string label, bool arg);
    
    virtual void execute_LineTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p1, int p2, Point3D line, double epsilon, int iterations, string label, bool arg);

	virtual void execute_DistTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p1, int p2, Point3D line, double epsilon, int iterations, string label, bool arg);

	virtual void execute_DistTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p1, int p2, int p3, Point3D line1, Point3D line2, Point3D line3, double epsilon, int iterations, string label, bool arg);
	
	virtual void execute_AngleTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p, Point3D line, Point3D point, double deltarad, int iterations, string label, bool arg);

	virtual void execute_RotTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p1, int p2, Point3D line, Point3D point, double deltarad, int iterations, string label, bool arg);

	virtual void execute_TetraedroA(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_TetraedroB(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_TetraedroC(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);

	virtual void execute_TetraedroD(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PiramideA(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PiramideB(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PiramideC(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PiramideD(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PiramideE(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PiramideF(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PiramideG(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PrismaA(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PrismaB(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PrismaC(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PrismaD(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PrismaE(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PrismaF(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PrismaG(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
	virtual void execute_PrismaH(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations);
	
private:
	virtual string itos(int i);
	
	virtual Point3D rotate_Point(Point3D p, double rad, Point3D line, Point3D point);
};
#endif
