#include "Test.h"

#define PI 3.14159265

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
Test::Test(){
    
}


//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
Test::~Test(){
    
}

void Test::writeGraphData(string name, vector<string> labels, vector<vector<double> > datos){
	FILE *file = fopen(name.c_str(),"w");
	
	if(labels.size() != datos.size()){
		printf("Error en el guardado de datos, labels y datos no calzan.");
	}
	
    for(int i = 0; i < labels.size(); i++){
		fprintf(file,"%s",labels[i].c_str());
		for(int j = 0; j < datos[i].size(); j++){
			fprintf(file," %f",datos[i][j]);
		}
		fprintf(file,"\n");
	}
	
	fclose(file);
}

void Test::writePointData(string name,vector<Point3D> &points, vector<vector<Point3D> > &pts, int &p,bool arg,int it){
	string pointsname = name + ".points";
	FILE *file = fopen(pointsname.c_str(),"w");
	
	for (unsigned int i=0;i<pts.size();i++){
		for(unsigned int j=0;j<pts[i].size();j++){
			if(arg){
				fprintf(file,"T ");
			}
			else{
				fprintf(file,"P ");
			}
			for (unsigned int k=0;k<points.size();k++){
				if(k==p){
					fprintf(file,"%f;%f;%f ",pts[i][j][0],pts[i][j][1],pts[i][j][2]);
				}else{
					fprintf(file,"%f;%f;%f ",points[k][0],points[k][1],points[k][2]);
				}
			}
			fprintf(file,"\n");
		}
	}
	fclose(file);
}

void Test::writePointData(string name,vector<Point3D> &points, vector<vector<Point3D> > &pts, int &p1, int &p2,bool arg,int it){
	string pointsname = name + ".points";
	FILE *file = fopen(pointsname.c_str(),"w");
	
	for(unsigned int j=0;j<it;j++){
		if(arg){
			fprintf(file,"T ");
		}
		else{
			fprintf(file,"P ");
		}
		for (unsigned int k=0;k<points.size();k++){
			if(k==p1){
				fprintf(file,"%f;%f;%f ",pts[0][j][0],pts[0][j][1],pts[0][j][2]);
			}else if (k==p2){
				fprintf(file,"%f;%f;%f ",pts[1][j][0],pts[1][j][1],pts[1][j][2]);
			}else{
				fprintf(file,"%f;%f;%f ",points[k][0],points[k][1],points[k][2]);
			}
		}
		fprintf(file,"\n");
	}
	
	fclose(file);
}

void Test::writePointData(string name,vector<Point3D> &points, vector<vector<Point3D> > &pts, int &p1, int& p2, int &p3 ,bool arg,int it){
	string pointsname = name + ".points";
	FILE *file = fopen(pointsname.c_str(),"w");
	
	for(unsigned int j=0;j<it;j++){
		if(arg){
			fprintf(file,"T ");
		}
		else{
			fprintf(file,"P ");
		}
		for (unsigned int k=0;k<points.size();k++){
			if(k==p1){
				fprintf(file,"%f;%f;%f ",pts[0][j][0],pts[0][j][1],pts[0][j][2]);
			}else if (k==p2){
				fprintf(file,"%f;%f;%f ",pts[1][j][0],pts[1][j][1],pts[1][j][2]);
			}else if (k==p3){
				fprintf(file,"%f;%f;%f ",pts[2][j][0],pts[2][j][1],pts[2][j][2]);
			}else{
				fprintf(file,"%f;%f;%f ",points[k][0],points[k][1],points[k][2]);
			}
		}
		fprintf(file,"\n");
	}
	
	fclose(file);
}

vector<vector<double> > Test::get_PointsQuality(vector<Point3D> &points, vector<Element *> &elements, vector<int> pts, vector<vector<Point3D> > coordinates, bool arg){
	vector<vector<double> > qua;
	
	if(arg){
		qua.resize(4);
	}else{
		qua.resize(3);
	}
	
	for(int i = 0; i < qua.size(); i++){
		qua[i].resize(coordinates[0].size());
	}
	
	Quality quality;
	
	for(int j = 0; j < qua[0].size(); j++){
		for(int i = 0; i < coordinates.size(); i++){
			points[pts[i]] = coordinates[i][j];
		}
		qua[0][j] = quality.get_JENS(points,elements);
		qua[1][j] = quality.get_JS(points,elements);
		qua[2][j] = quality.get_JRATIO(points,elements);
		if(arg){
			qua[3][j] = quality.get_ARG(points,elements);
		}
	}
	
	return qua;
}

void Test::execute_LineTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p, Point3D line, double epsilon, int iterations, string label, bool arg){
	vector<int> pt(1,p);
	
	vector<vector<Point3D> > pts(1);
	pts[0].resize(iterations);
	
	for(int i = 0; i < iterations; i++){
		pts[0][i] = points[p] + (line*(epsilon*i));
	}
	
	writePointData(name,points,pts,p,arg,iterations);
	
	vector<vector<double> > qua;
	
	qua = get_PointsQuality(points,elements,pt,pts,arg);
	
	vector<vector<double> > datos;
	vector<string> labels;
	
	if(arg){
		datos.resize(5);
		labels.resize(5);
		datos[4].resize(iterations);
		labels[4]="ARG";
	}else{
		datos.resize(4);
		labels.resize(4);
	}
	
	datos[0].resize(iterations);
	datos[1].resize(iterations);
	datos[2].resize(iterations);
	datos[3].resize(iterations);
	
	labels[0]=label;
	labels[1]="$\\mathsf{J_{ENS}}$";
	labels[2]="$\\mathsf{J_S}$";
	labels[3]="$\\mathsf{J_R}$";
	
	for(int i = 0; i < iterations; i++){
		datos[0][i]=(line*(epsilon*i)).Norm();
		datos[1][i]=qua[0][i];
		datos[2][i]=qua[1][i];
		datos[3][i]=qua[2][i];
		if(arg){
			datos[4][i]=qua[3][i];
		}
	}
	
	writeGraphData(name,labels,datos);
}

void Test::execute_LineTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p1, int p2, Point3D line, double epsilon, int iterations, string label, bool arg){
	vector<int> pt(2,p1);
	pt[1] = p2;
	
	vector<vector<Point3D> > pts(2);
	pts[0].resize(iterations);
	pts[1].resize(iterations);
	
	for(int i = 0; i < iterations; i++){
		pts[0][i] = points[p1] + (line*(epsilon*i));
		pts[1][i] = points[p2] + (line*(epsilon*i));
	}
	
	writePointData(name,points,pts,p1,p2,arg,iterations);
	
	vector<vector<double> > qua;
	
	qua = get_PointsQuality(points,elements,pt,pts,arg);
	
	vector<vector<double> > datos;
	vector<string> labels;
	
	if(arg){
		datos.resize(5);
		labels.resize(5);
		datos[4].resize(iterations);
		labels[4]="ARG";
	}else{
		datos.resize(4);
		labels.resize(4);
	}
	
	datos[0].resize(iterations);
	datos[1].resize(iterations);
	datos[2].resize(iterations);
	datos[3].resize(iterations);
	
	labels[0]=label;
	labels[1]="$\\mathsf{J_{ENS}}$";
	labels[2]="$\\mathsf{J_S}$";
	labels[3]="$\\mathsf{J_R}$";
	
	for(int i = 0; i < iterations; i++){
		datos[0][i]=(line*(epsilon*i)).Norm();
		datos[1][i]=qua[0][i];
		datos[2][i]=qua[1][i];
		datos[3][i]=qua[2][i];
		if(arg){
			datos[4][i]=qua[3][i];
		}
	}
	
	writeGraphData(name,labels,datos);
}

void Test::execute_DistTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p1, int p2, Point3D line, double epsilon, int iterations, string label, bool arg){
	vector<int> pt(2,p1);
	pt[1] = p2;
	
	vector<vector<Point3D> > pts(2);
	pts[0].resize(iterations);
	pts[1].resize(iterations);
	
	for(int i = 0; i < iterations; i++){
		pts[0][i] = points[p1] - (line*(epsilon*i));
		pts[1][i] = points[p2] + (line*(epsilon*i));
	}
	
	writePointData(name,points,pts,p1,p2,arg,iterations);
	
	vector<vector<double> > qua;
	
	qua = get_PointsQuality(points,elements,pt,pts,arg);
	
	vector<vector<double> > datos;
	vector<string> labels;
	
	if(arg){
		datos.resize(5);
		labels.resize(5);
		datos[4].resize(iterations);
		labels[4]="ARG";
	}else{
		datos.resize(4);
		labels.resize(4);
	}
	
	datos[0].resize(iterations);
	datos[1].resize(iterations);
	datos[2].resize(iterations);
	datos[3].resize(iterations);
	
	labels[0]=label;
	labels[1]="$\\mathsf{J_{ENS}}$";
	labels[2]="$\\mathsf{J_S}$";
	labels[3]="$\\mathsf{J_R}$";
	
	for(int i = 0; i < iterations; i++){
		datos[0][i]=pts[0][i].distance(pts[1][i]);
		datos[1][i]=qua[0][i];
		datos[2][i]=qua[1][i];
		datos[3][i]=qua[2][i];
		if(arg){
			datos[4][i]=qua[3][i];
		}
	}
	
	writeGraphData(name,labels,datos);
}

void Test::execute_DistTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p1, int p2, int p3, Point3D line1, Point3D line2, Point3D line3, double epsilon, int iterations, string label, bool arg){
	vector<int> pt(3,p1);
	pt[1] = p2;
	pt[2] = p3;
	
	vector<vector<Point3D> > pts(3);
	pts[0].resize(iterations);
	pts[1].resize(iterations);
	pts[2].resize(iterations);
	
	for(int i = 0; i < iterations; i++){
		pts[0][i] = points[p1] + (line1*(epsilon*i));
		pts[1][i] = points[p2] + (line2*(epsilon*i));
		pts[2][i] = points[p3] + (line3*(epsilon*i));
	}
	
	writePointData(name,points,pts,p1,p2,p3,arg,iterations);
	
	vector<vector<double> > qua;
	
	qua = get_PointsQuality(points,elements,pt,pts,arg);
	
	vector<vector<double> > datos;
	vector<string> labels;
	
	if(arg){
		datos.resize(5);
		labels.resize(5);
		datos[4].resize(iterations);
		labels[4]="ARG";
	}else{
		datos.resize(4);
		labels.resize(4);
	}
	
	datos[0].resize(iterations);
	datos[1].resize(iterations);
	datos[2].resize(iterations);
	datos[3].resize(iterations);
	
	labels[0]=label;
	labels[1]="$\\mathsf{J_{ENS}}$";
	labels[2]="$\\mathsf{J_S}$";
	labels[3]="$\\mathsf{J_R}$";
	
	for(int i = 0; i < iterations; i++){
		datos[0][i]=pts[0][i].distance(Point3D(0,0,0));
		datos[1][i]=qua[0][i];
		datos[2][i]=qua[1][i];
		datos[3][i]=qua[2][i];
		if(arg){
			datos[4][i]=qua[3][i];
		}
	}
	
	writeGraphData(name,labels,datos);
}

void Test::execute_AngleTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p, Point3D line, Point3D point, double deltarad, int iterations, string label, bool arg){
	vector<int> pt(1,p);
	
	vector<vector<Point3D> > pts(1);
	pts[0].resize(iterations);
	for(int i = 1; i < iterations; i++){
		pts[0][i] = rotate_Point(points[p],deltarad*double(i),line,point);
	}
	
	writePointData(name,points,pts,p,arg,iterations);
	
	vector<vector<double> > qua;
	
	qua = get_PointsQuality(points,elements,pt,pts,arg);
	
	vector<vector<double> > datos;
	vector<string> labels;
	
	if(arg){
		datos.resize(5);
		labels.resize(5);
		datos[4].resize(iterations);
		labels[4]="ARG";
	}else{
		datos.resize(4);
		labels.resize(4);
	}
	
	datos[0].resize(iterations);
	datos[1].resize(iterations);
	datos[2].resize(iterations);
	datos[3].resize(iterations);
	
	labels[0]=label;
	labels[1]="$\\mathsf{J_{ENS}}$";
	labels[2]="$\\mathsf{J_S}$";
	labels[3]="$\\mathsf{J_R}$";
	
	for(int i = 0; i < iterations; i++){
		datos[0][i]=(180*deltarad*double(i))/PI - 90;
		datos[1][i]=qua[0][i];
		datos[2][i]=qua[1][i];
		datos[3][i]=qua[2][i];
		if(arg){
			datos[4][i]=qua[3][i];
		}
	}
	
	writeGraphData(name,labels,datos);
}

void Test::execute_RotTest(vector<Point3D> &points, vector<Element *> &elements, string name, int p1, int p2, Point3D line, Point3D point, double deltarad, int iterations, string label, bool arg){
	vector<int> pt(2,p1);
	pt[1] = p2;
	
	vector<vector<Point3D> > pts(2);
	pts[0].resize(iterations);
	pts[1].resize(iterations);
	
	for(int i = 0; i < iterations; i++){
		pts[0][i] = rotate_Point(points[p1],deltarad*double(i),line,point);
		pts[1][i] = rotate_Point(points[p2],deltarad*double(i),line,point);
	}
	
	writePointData(name,points,pts,p1,p2,arg,iterations);
	
	vector<vector<double> > qua;
	
	qua = get_PointsQuality(points,elements,pt,pts,arg);
	
	vector<vector<double> > datos;
	vector<string> labels;
	
	if(arg){
		datos.resize(5);
		labels.resize(5);
		datos[4].resize(iterations);
		labels[4]="ARG";
	}else{
		datos.resize(4);
		labels.resize(4);
	}
	
	datos[0].resize(iterations);
	datos[1].resize(iterations);
	datos[2].resize(iterations);
	datos[3].resize(iterations);
	
	labels[0]=label;
	labels[1]="$\\mathsf{J_{ENS}}$";
	labels[2]="$\\mathsf{J_S}$";
	labels[3]="$\\mathsf{J_R}$";
	
	for(int i = 0; i < iterations; i++){
		datos[0][i]=(180*deltarad*double(i))/PI - 90;
		datos[1][i]=qua[0][i];
		datos[2][i]=qua[1][i];
		datos[3][i]=qua[2][i];
		if(arg){
			datos[4][i]=qua[3][i];
		}
	}
	
	writeGraphData(name,labels,datos);
}

Point3D Test::rotate_Point(Point3D p, double rad, Point3D line, Point3D point){
	line.normalize();
	
	double sine = sin(rad);
	double cosine = cos(rad);
	
	double a,b,c,u,v,w,x,y,z;
	
	a = point[0];
	b = point[1];
	c = point[2];
	u = line[0];
	v = line[1];
	w = line[2];
	x = p[0];
	y = p[1];
	z = p[2];
	
	Point3D result;
	
	result[0] = (a*(v*v + w*w) - u*(b*v + c*w - u*x - v*y - w*z))*(1-cosine) + x*cosine + (-c*v + b*w - w*y + v*z)*sine;
	result[1] = (b*(u*u + w*w) - v*(a*u + c*w - u*x - v*y - w*z))*(1-cosine) + y*cosine + (c*u - a*w + w*x - u*z)*sine;
	result[2] = (c*(u*u + v*v) - w*(a*u + b*v - u*x - v*y - w*z))*(1-cosine) + z*cosine + (-b*u + a*v - v*x + u*y)*sine;
	
	return result;
}

string Test::itos(int i){
    stringstream s;
    s << i;
    return s.str();
}
    
void Test::execute_TetraedroA(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	double epsilon = (4.0*(points[3][1]))/(double(iterations)-1);
	points[3][1] = 0;
	Point3D line = Point3D(0,1,0);
	
	execute_LineTest(points,elements,name,3,line,epsilon,iterations,"Height",true);
}

void Test::execute_TetraedroB(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p1 = 0;
	int p2 = 2;
	
	Point3D line = (points[p2]-points[p1]);
	line.normalize();
	
	double distancia = points[p1].distance(points[p2]);
	
	double epsilon = (2.0*distancia)/(double(iterations)-1);
	
	points[p1] = points[p1] + ((points[p2]-points[p1])/2.0);
	points[p2] = points[p1];
	execute_DistTest(points,elements,name,p1,p2,line,epsilon,iterations,"Distance",true);
}

void Test::execute_TetraedroC(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p = 3;
	Point3D line = Point3D(-1,0,0);
	Point3D point = Point3D(0,0,points[p][2]);
	double deltarad = PI/(double(iterations)-1);
	points[p] = rotate_Point(points[p],-0.5*PI,line,point);
	execute_AngleTest(points,elements,name,p,line,point,deltarad,iterations,"Rotation_Angle",true);
}

void Test::execute_TetraedroD(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p1 = 0;
	int p2 = 2;
	Point3D point = (points[p1]+((points[p2]-points[p1])/2.0));
	
	Point3D line = point -(points[1]+((points[3]-points[1])/2.0));
	double deltarad = (0.5*PI)/(double(iterations)-1);
	execute_RotTest(points,elements,name,p1,p2,line,point,deltarad,iterations,"Rotation_Angle",true);
}

void Test::execute_PiramideA(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p = 4;
	Point3D line = Point3D(0,0,1);
	Point3D point = Point3D(0,0,0);
	double deltarad = PI/(double(iterations)-1);
	points[p] = rotate_Point(points[p],-0.5*PI,line,point);
	
	execute_AngleTest(points,elements,name,p,line,point,deltarad,iterations,"Rotation_Angle",false);
}

void Test::execute_PiramideB(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p = 4;
	Point3D line = Point3D(1,0,1);
	line.normalize();
	Point3D point = Point3D(0,0,0);
	double deltarad = PI/(double(iterations)-1);
	points[p] = rotate_Point(points[p],-0.5*PI,line,point);
	
	execute_AngleTest(points,elements,name,p,line,point,deltarad,iterations,"Rotation_Angle",false);
}

void Test::execute_PiramideC(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	double epsilon = (4.0*((points[4]).distance(Point3D(0,0,0))))/(double(iterations)-1);
	points[4][1] = 0;
	Point3D line = Point3D(0,1,0);
	line.normalize();
	
	execute_LineTest(points,elements,name,4,line,epsilon,iterations,"Height",false);
}

void Test::execute_PiramideD(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	double epsilon = (4.0*((points[0]).distance(Point3D(0,0,0))))/(double(iterations)-1);
	points[0][0] = 0;
	points[0][1] = 0;
	points[0][2] = 0;
	Point3D line = Point3D(-1,0,-1);
	line.normalize();
	
	execute_LineTest(points,elements,name,0,line,epsilon,iterations,"Distance_to_Base_Center",false);
}

void Test::execute_PiramideE(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p1 = 0;
	int p2 = 1;
	
	Point3D line = (points[p2]-points[p1]);
	line.normalize();
	
	double distancia = points[p1].distance(points[p2]);
	
	double epsilon = (2.0*distancia)/(double(iterations)-1);
	
	points[p1] = points[p1] + ((points[p2]-points[p1])/2.0);
	points[p2] = points[p1];
	execute_DistTest(points,elements,name,p1,p2,line,epsilon,iterations,"Distance",false);
}

void Test::execute_PiramideF(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p1 = 0;
	int p2 = 2;
	
	Point3D line = (points[p2]-points[p1]);
	line.normalize();
	
	double distancia = points[p1].distance(points[p2]);
	
	double epsilon = (2.0*distancia)/(double(iterations)-1);
	
	points[p1] = points[p1] + ((points[p2]-points[p1])/2.0);
	points[p2] = points[p1];
	execute_DistTest(points,elements,name,p1,p2,line,epsilon,iterations,"Distance",false);
}

void Test::execute_PiramideG(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p1 = 0;
	int p2 = 1;
	int p3 = 2;
	
	Point3D line1 = (points[p1]-Point3D(0,0,0));
	Point3D line2 = (points[p2]-Point3D(0,0,0));
	Point3D line3 = (points[p3]-Point3D(0,0,0));
	line1.normalize();
	line2.normalize();
	line3.normalize();
	
	double epsilon = (4.0*((points[p1]).distance(Point3D(0,0,0))))/(double(iterations)-1);
	points[p1] = Point3D(0,0,0);
	points[p2] = Point3D(0,0,0);
	points[p3] = Point3D(0,0,0);
	
	execute_DistTest(points,elements,name,p1,p2,p3,line1,line2,line3,epsilon,iterations,"Distance_to_Base_Center",false);
}


void Test::execute_PrismaA(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p = 4;
	Point3D p1 = points[0]+((points[2] - points[0])/2);
	Point3D p2 = points[3]+((points[5] - points[3])/2);
	Point3D line = p2 - p1;
	line.normalize();
	double deltarad = PI/(double(iterations)-1);
	points[p] = rotate_Point(points[p],-0.5*PI,line,p1);
	
	execute_AngleTest(points,elements,name,p,line,p1,deltarad,iterations,"Rotation_Angle",false);
}

void Test::execute_PrismaB(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p1 = 1;
	int p2 = 4;
	Point3D p3 = points[0]+((points[2] - points[0])/2);
	Point3D p4 = points[3]+((points[5] - points[3])/2);
	Point3D line = p4 - p3;
	line.normalize();
	double deltarad = PI/(double(iterations)-1);
	points[p1] = rotate_Point(points[p1],-0.5*PI,line,p3);
	points[p2] = rotate_Point(points[p2],-0.5*PI,line,p4);
	
	execute_RotTest(points,elements,name,p1,p2,line,p3,deltarad,iterations,"Rotation_Angle",false);
}

void Test::execute_PrismaC(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	Point3D line = points[4] - points[3];
	double epsilon = (4.0*(line.Norm()))/(double(iterations)-1);
	points[4] = points[3];
	line.normalize();
	
	execute_LineTest(points,elements,name,4,line,epsilon,iterations,"Distance_to_Static_Point",false);
}

void Test::execute_PrismaD(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	Point3D line = points[4] - points[3];
	double epsilon = (4.0*(line.Norm()))/(double(iterations)-1);
	points[4] = points[3];
	points[1] = points[0];
	line.normalize();
	
	execute_LineTest(points,elements,name,1,4,line,epsilon,iterations,"Distance_to_Static_Points",false);
}

void Test::execute_PrismaE(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	Point3D p1 = points[3]+((points[5] - points[3])/2);
	Point3D line = points[4] - p1;
	double epsilon = (4.0*(line.Norm()))/(double(iterations)-1);
	points[4] = p1;
	line.normalize();
	
	execute_LineTest(points,elements,name,4,line,epsilon,iterations,"Height",false);
}

void Test::execute_PrismaF(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	Point3D p1 = points[0]+((points[2] - points[0])/2);
	Point3D p2 = points[3]+((points[5] - points[3])/2);
	Point3D line = points[4] - p2;
	double epsilon = (4.0*(line.Norm()))/(double(iterations)-1);
	points[1] = p1;
	points[4] = p2;
	line.normalize();
	
	execute_LineTest(points,elements,name,1,4,line,epsilon,iterations,"Height",false);
}

void Test::execute_PrismaG(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	Point3D line = points[0] - points[3];
	double epsilon = (4.0*(line.Norm()))/(double(iterations)-1);
	points[0] = points[3];
	line.normalize();
	
	execute_LineTest(points,elements,name,0,line,epsilon,iterations,"Distance_to_Static_Point",false);
}

void Test::execute_PrismaH(vector<Point3D> &points, vector<Element *> &elements, string name, int iterations){
	int p1 = 1;
	int p2 = 4;
	
	Point3D line = (points[p2]-points[p1]);
	line.normalize();
	
	double distancia = points[p1].distance(points[p2]);
	
	double epsilon = (2.0*distancia)/(double(iterations)-1);
	
	points[p1] = points[p1] + ((points[p2]-points[p1])/2.0);
	points[p2] = points[p1];
	
	execute_DistTest(points,elements,name,p1,p2,line,epsilon,iterations,"Distance",false);
}
