import numpy as np
import math
import matplotlib
import matplotlib.pyplot as plt
from sys import argv

# tamano de la fuente de los graficos
matplotlib.rcParams.update({'font.size': 23})

# formato de los puntos del grafico
markers = ['o', 'v', 's', 'd']
colors = ['blue', 'green', 'red', 'cyan']

argumentos = argv
for i in range(2):
	script = argumentos[0]
	argumentos.remove(script)
height = int(len(argumentos)+1)/2
fig = plt.figure(1)
i = 1
for filename in argumentos:
	fig = plt.figure(i)
	fig.canvas.set_window_title('Test '+chr(ord('A')+i-1))

	txt = open(filename)

	lines = txt.readlines()

	labels = list()
	data = list()
	x = True
	for line in lines:
		l = line.split(' ')
		d = list()
		primero = True
		for com in l:
			if primero:
				labels.append(com)
				primero = False
			else:
				if x or (float(com) <= 1 and float(com) >= 0):
					d.append(float(com))
					x = True
		data.append(d)
		x = False

	if(len(data) < 1):
		print('error in data')
		exit()
	elif(len(data) == 1):
		plt.plot(data[0], marker=markers[0], color=colors[0], label=labels[0], markersize=12, linewidth=3)
	elif (len(data) == 2):
		plt.plot(data[0], data[1], marker=markers[0], color=colors[0], label=labels[1], markersize=12, linewidth=3)
	elif (len(data) == 3):
		plt.plot(data[0], data[1], marker=markers[0], color=colors[0], label=labels[1], markersize=12, linewidth=3)
		plt.plot(data[0], data[2], marker=markers[1], color=colors[1], label=labels[2], markersize=12, linewidth=3)
	elif (len(data) == 4):
		plt.plot(data[0][(len(data[0])-len(data[1])):], data[1], marker=markers[0], color=colors[0], label=labels[1], markersize=12, linewidth=3)
		plt.plot(data[0][(len(data[0])-len(data[2])):], data[2], marker=markers[1], color=colors[1], label=labels[2], markersize=12, linewidth=3)
		plt.plot(data[0][(len(data[0])-len(data[3])):], data[3], marker=markers[2], color=colors[2], label=labels[3], markersize=12, linewidth=3)
	else:
		plt.plot(data[0][(len(data[0])-len(data[1])):], data[1], marker=markers[0], color=colors[0], label=labels[1], markersize=12, linewidth=3)
		plt.plot(data[0][(len(data[0])-len(data[2])):], data[2], marker=markers[1], color=colors[1], label=labels[2], markersize=12, linewidth=3)
		plt.plot(data[0][(len(data[0])-len(data[3])):], data[3], marker=markers[2], color=colors[2], label=labels[3], markersize=12, linewidth=3)
		plt.plot(data[0][(len(data[0])-len(data[4])):], data[4], marker=markers[3], color=colors[3], label=labels[4], markersize=12, linewidth=3)
	plt.ylabel('Quality')
	if(len(data) == 1):
		plt.xlabel('')
	else:
		plt.xlabel(labels[0].replace('_',' '))
		plt.grid(True)
		plt.legend(loc='best', prop={'size':23}, numpoints=2)
		plt.subplots_adjust(top=0.95, bottom=0.17,right=0.97, left=0.19)
	print("Plotter: save graphic in file "+script+chr(ord('A')+i-1)+"R.pdf")
	plt.savefig(script+chr(ord('A')+i-1)+"R.pdf",format="pdf")
	i = i+1
#plt.show()

