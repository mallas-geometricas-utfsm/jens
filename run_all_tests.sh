date "+%H:%M:%S"
cmake ./src
make
./run_tetrahedron_tests.sh $1
./run_prism_tests.sh $2
./run_pyramid_tests.sh $3
date "+%H:%M:%S"
